package com.example.nagat.pageclone.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.model.Passion;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nagat on 18/4/2018.
 */

public class GrillViewChosePassionAdapter extends BaseAdapter {
    private List<Passion> mListPaper;
    private List<Passion> mListPaperChose;
    private LayoutInflater mLayoutInflater;
    private Context context;
    private OnClickFlipView onClickFlipView;

    public void setOnClickFlipView(OnClickFlipView onClickFlipView) {
        this.onClickFlipView = onClickFlipView;
    }

    public GrillViewChosePassionAdapter(Context context, List<Passion> listPaper,List<Passion> mListPaperChose) {
        mLayoutInflater = LayoutInflater.from(context);
        mListPaper = listPaper;
        this.mListPaperChose = mListPaperChose;
        this.context = context;
    }

    @Override
    public int getCount() {
        return mListPaper == null ? 0 : mListPaper.size();
    }

    @Override
    public Object getItem(int position) {
        return mListPaper.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewTextHolder viewTextHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.item_newspaper,
                    parent, false);
            viewTextHolder = new ViewTextHolder();
            viewTextHolder.mImageView = convertView.findViewById(R.id.avatarNewspaper);
            viewTextHolder.mTextView = (TextView) convertView.findViewById(R.id.nameNewspaper);
            viewTextHolder.relativeLayout = (RelativeLayout) convertView.findViewById(R.id.rlAll);
            convertView.setTag(viewTextHolder);

        } else {
            viewTextHolder = (ViewTextHolder) convertView.getTag();
        }
        viewTextHolder.mTextView.setText(mListPaper.get(position).getNamePaper());
//        viewTextHolder.mImageView.setImageResource(mListPaper.get(position).getImage());
        Picasso.get()
                .load(mListPaper.get(position).getImage())
                .placeholder(R.drawable.ic_white_2x)
                .error(R.drawable.ic_white_2x)
                .resize(65, 65)
                .centerCrop()
                .into(viewTextHolder.mImageView);
        if (mListPaperChose.contains(mListPaper.get(position))) {
            viewTextHolder.relativeLayout.setBackgroundResource(R.drawable.border);
        } else {
            viewTextHolder.relativeLayout.setBackgroundResource(R.drawable.border_non);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {

                if (viewTextHolder.relativeLayout.getBackground().getConstantState().equals(ContextCompat.getDrawable(context, R.drawable.border).getConstantState())) {
                    viewTextHolder.relativeLayout.setBackgroundResource(R.drawable.border_non);
                    onClickFlipView.onClickGridView(Constant.TYPE_GRIDVIEW_PASSION, position, false, mListPaper.get(position));
                } else {
                    viewTextHolder.relativeLayout.setBackgroundResource(R.drawable.border);
                    onClickFlipView.onClickGridView(Constant.TYPE_GRIDVIEW_PASSION, position, true, mListPaper.get(position));
                }

            }

        });
        return convertView;

    }

    private class ViewTextHolder {
        private TextView mTextView;
        private CircleImageView mImageView;
        private RelativeLayout relativeLayout;
    }
}

