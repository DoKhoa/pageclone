package com.example.nagat.pageclone.ultis;

/**
 * Created by nagat on 2/5/2018.
 */

public class Constant {
    public static final String LOGIN_FB = "login_with_facebook";
    public static final String LOGIN = "login";
    public static final int BUTTON_BATDAU = 0;
    public static final int BUTTON_DANGNHAP = 1;
    public static final String FIRST_TIME_RUN = "first_time_run";
    public static final int TYPE_GRIDVIEW_SITE = 0;
    public static final int TYPE_GRIDVIEW_PASSION = 1;
    public static final String LIST_PASSION_CHOSE = "list_passion_chose";
    public static final String LIST_SITE_CHOSE = "list_site_chose";
    public static final String LIST_PASSION = "list_passion";
    public static final String LIST_SITE = "list_site";
    //danh muc cac chu de quan tam
    public static final String VAN_HOA = "van_hoa";
    public static final String AM_THUC = "am_thuc";
    public static final String THE_THAO = "the_thao";
    public static final String VAN_NGHE = "van_nghe";
    public static final String KINH_TE = "kinh_te";
    public static final String PHAP_LUAT = "phap_luat";
    public static final String GIAI_TRI = "giai_tri";
    public static final String CONG_NGHE = "cong_nghe";
    public static final String CHINH_TRI = "chinh_tri";
    //api rss2json
    public static final String API = "cwwufiibuwaoqxrpmsbilotjnk3teawuz2wcf45a";
    public static final String URL_RSS2JSON = "https://api.rss2json.com/v1/api.json";
    public static final String ORDER_BY_DATE = "pubDate";
    public static final String ORDER_BY_TITLE = "title";
    public static final String ORDER_BY_AUTHOR = "author";
    public static final String ORDER_DIR_ASC = "asc";
    public static final String ORDER_DIR_DESC = "desc";

    //danhmuc cac site quan tam
    public static final String DAN_TRI = "dan_tri";
    public static final String TIEN_PHONG = "tien_phong";
    public static final String VNEXPRESS = "vnexpress";
    public static final String VIET_BAO = "viet_bao";
    public static final String BAO_PHAP_LUAT = "phap_luat";
    public static final String VTC = "vtc";
    public static final String THANH_NIEN = "thanh_nien";
    public static final String BAO_24H = "24h";
    public static final String SOHA = "so_ha";

    //
    public static final String PASSION = "passion";
}
