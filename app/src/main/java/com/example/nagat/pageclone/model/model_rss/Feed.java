package com.example.nagat.pageclone.model.model_rss;

/**
 * Created by nagat on 8/5/2018.
 */

public class Feed {

    private String title;


    public Feed() {
    }

    public Feed(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
