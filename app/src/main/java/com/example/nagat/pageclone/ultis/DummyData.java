package com.example.nagat.pageclone.ultis;

import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.model.Passion;
import com.example.nagat.pageclone.model.Site;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nagat on 3/5/2018.
 */

public class DummyData {

    public List<Site> site_list;
    public List<Passion> passion_list;

//    public static List<Passion> createDataPassion() {
//        List<Passion> mListPassion;
//        mListPassion = new ArrayList<>();
//        mListPassion.add(new Passion( "Pháp luật", R.drawable.bieutuong_phapluat,Constant.PHAP_LUAT));
//        mListPassion.add(new Passion("Chính trị", R.drawable.ic_white_2x,Constant.CHINH_TRI));
//        mListPassion.add(new Passion("Giải trí", R.drawable.logo_giaitri,Constant.GIAI_TRI));
//        mListPassion.add(new Passion("Kinh tế", R.drawable.bieutuong_kinhte,Constant.KINH_TE));
//        mListPassion.add(new Passion( "Ẩm thực", R.drawable.bieutuong_amthuc,Constant.AM_THUC));
//        mListPassion.add(new Passion("Văn nghệ", R.drawable.bieutuong_vannghe,Constant.VAN_NGHE));
//        mListPassion.add(new Passion("Văn hóa", R.drawable.bieutuong_vanhoa,Constant.VAN_HOA));
//        mListPassion.add(new Passion("Công nghệ", R.drawable.bieutuong_congnghe,Constant.CONG_NGHE));
//        mListPassion.add(new Passion("Thể Thao", R.drawable.bieutuong_thethao,Constant.THE_THAO));
//        return mListPassion;
//    }
//    public static List<Site> createDataSite() {
//        List<Site> mListSite;
//        mListSite = new ArrayList<>();
//        HashMap<String,String> hashMap1 = new HashMap<String, String>();
//        hashMap1.put(Constant.PHAP_LUAT,"http://dantri.com.vn/phap-luat.rss");
//        hashMap1.put(Constant.CHINH_TRI,"http://dantri.com.vn/xa-hoi/chinh-tri.rss");
//        hashMap1.put(Constant.GIAI_TRI,"http://dantri.com.vn/giai-tri.rss");
//        hashMap1.put(Constant.KINH_TE,"http://dantri.com.vn/kinh-doanh.rss");
//        hashMap1.put(Constant.AM_THUC,"http://dantri.com.vn/doi-song/bep-ngon.rss");
//        hashMap1.put(Constant.VAN_NGHE,"http://dantri.com.vn/van-hoa/dien-anh.rss");
//        hashMap1.put(Constant.VAN_HOA,"http://dantri.com.vn/van-hoa.rss");
//        hashMap1.put(Constant.CONG_NGHE,"http://dantri.com.vn/khoa-hoc-cong-nghe.rss");
//        hashMap1.put(Constant.THE_THAO,"http://dantri.com.vn/the-thao.rss");
//        Site site1 = new Site("Dân chí", R.drawable.logo_dantri,Constant.DAN_TRI,hashMap1);
//        mListSite.add(site1);
//        mListSite.add(new Site("Tiền Phong", R.drawable.logo_tienphong,Constant.TIEN_PHONG,new HashMap<String, String>()));
//
//
//
//        HashMap<String,String> hashMap2 = new HashMap<String, String>();
//        hashMap2.put(Constant.PHAP_LUAT,"https://vnexpress.net/rss/phap-luat.rss");
//        hashMap2.put(Constant.CHINH_TRI,"https://vnexpress.net/rss/thoi-su.rss");
//        hashMap2.put(Constant.GIAI_TRI,"https://vnexpress.net/rss/giai-tri.rss");
//        hashMap2.put(Constant.KINH_TE,"https://vnexpress.net/rss/kinh-doanh.rss");
//        hashMap2.put(Constant.AM_THUC,"https://vnexpress.net/rss/gia-dinh.rss");
//        hashMap2.put(Constant.VAN_NGHE,"https://vnexpress.net/rss/tam-su.rss");
//        hashMap2.put(Constant.VAN_HOA,"https://vnexpress.net/rss/giao-duc.rss");
//        hashMap2.put(Constant.CONG_NGHE,"https://vnexpress.net/rss/so-hoa.rss");
//        hashMap2.put(Constant.THE_THAO,"https://vnexpress.net/rss/the-thao.rss");
//        Site site2 = new Site("Vnexpress",  R.drawable.logo_vnexpess,Constant.VNEXPRESS,hashMap2);
//        mListSite.add(site2);
//
//        mListSite.add(new Site("Việt Báo", R.drawable.logo_vietbao,Constant.VIET_BAO,new HashMap<String, String>()));
//        mListSite.add(new Site( "Pháp Luật", R.drawable.logo_phapluat,Constant.BAO_PHAP_LUAT,new HashMap<String, String>()));
//        mListSite.add(new Site("VTC", R.drawable.logo_vtc,Constant.VTC,new HashMap<String, String>()));
//        mListSite.add(new Site("Thanh niên", R.drawable.logo_thanhnien,Constant.THANH_NIEN,new HashMap<String, String>()));
//        mListSite.add(new Site("24H", R.drawable.logo_24h,Constant.BAO_24H,new HashMap<String, String>()));
//        mListSite.add(new Site("Soha", R.drawable.logo_soha,Constant.SOHA,new HashMap<String, String>()));
//
//        return mListSite;
//    }
}
