package com.example.nagat.pageclone.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.nagat.pageclone.BaseActivity;
import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.fragment.FragmentArticleSaved;
import com.example.nagat.pageclone.fragment.FragmentChosePassion;
import com.example.nagat.pageclone.fragment.FragmentChoseSite;
import com.example.nagat.pageclone.fragment.FragmentInfor;
import com.example.nagat.pageclone.fragment.FragmentMain;
import com.example.nagat.pageclone.model.User;
import com.example.nagat.pageclone.realm.RealmController;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.example.nagat.pageclone.ultis.Ultis;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.Login;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    //    private ArrayList<Passion> genresList;
    private static final String TAG = "MainActivity";
    Menu menu;
    private boolean isLogin = false;
    NavigationView navigationView;
    Realm realm;
    private boolean isChangeInfor = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        realm = RealmController.getInstance().with(this).getRealm();
        SharedPrefs.getInstance().put(Constant.FIRST_TIME_RUN,false);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        this.setupUI(this.getWindow().getDecorView().findViewById(android.R.id.content));
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        isLogin = SharedPrefs.getInstance().get(Constant.LOGIN, Boolean.class, false);
        if (isLogin) {
            visibleItem();
        } else {
            hideItem();
        }
        menu = navigationView.getMenu();


        FragmentMain fragmentMain = new FragmentMain();
        Ultis.tranformFragment(this, R.id.full, fragmentMain);
        Log.e(TAG,"oncreate");
    }
    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }
    private void hideItem() {
        navigationView.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_infor_user).setVisible(false);
        nav_Menu.findItem(R.id.nav_logout).setVisible(false);
        nav_Menu.findItem(R.id.nav_article_saved).setVisible(false);
    }

    private void visibleItem() {
        navigationView.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentInfor fragmentInfor = new FragmentInfor();
                Ultis.tranformFragment(MainActivity.this, R.id.full, fragmentInfor);
                choseItem(R.id.nav_infor_user);
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_infor_user).setVisible(true);
        nav_Menu.findItem(R.id.nav_logout).setVisible(true);
        nav_Menu.findItem(R.id.nav_article_saved).setVisible(true);
    }


    public void choseItem(int itemId) {
        menu.findItem(itemId).setChecked(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    public void setHeader(String displayName, String email, File logo) {
        TextView name = navigationView.getHeaderView(0).findViewById(R.id.view1);
        TextView emailDisplay = navigationView.getHeaderView(0).findViewById(R.id.email);
        CircleImageView imageView = navigationView.getHeaderView(0).findViewById(R.id.imageView);
        name.setText(displayName);
        emailDisplay.setText(email);
        Picasso.get()
                .load(logo)
                .placeholder(R.drawable.logo)
                .resize(65,65)
                .centerCrop()
                .error(R.drawable.ic_white_2x)
                .into(imageView);

    }
    public void setHeader(String displayName, String email, byte[] logo) {
        TextView name = navigationView.getHeaderView(0).findViewById(R.id.view1);
        TextView emailDisplay = navigationView.getHeaderView(0).findViewById(R.id.email);
        CircleImageView imageView = navigationView.getHeaderView(0).findViewById(R.id.imageView);
        name.setText(displayName);
        emailDisplay.setText(email);
        byte[] avatarData = logo;
        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(avatarData, 0, avatarData.length);
        imageView.setImageBitmap(decodeByteArray);

    }
    public void setHeader(String displayName, String email) {
        TextView name = navigationView.getHeaderView(0).findViewById(R.id.view1);
        TextView emailDisplay = navigationView.getHeaderView(0).findViewById(R.id.email);
        CircleImageView imageView = navigationView.getHeaderView(0).findViewById(R.id.imageView);
        name.setText(displayName);
        emailDisplay.setText(email);

    }

    public void setHeader(String displayName, String email, int logo) {
        TextView name = navigationView.getHeaderView(0).findViewById(R.id.view1);
        TextView emailDisplay = navigationView.getHeaderView(0).findViewById(R.id.email);
        CircleImageView imageView = navigationView.getHeaderView(0).findViewById(R.id.imageView);
        name.setText(displayName);
        emailDisplay.setText(email);
        Picasso.get()
                .load(logo)
                .placeholder(R.drawable.logo)
                .error(R.drawable.ic_white_2x)
                .into(imageView);

    }

    public void initUI(Boolean login) {
        if (login) {
            visibleItem();
            User user = RealmController.with(this).getUser(Ultis.getEmailUser());
//            Log.e(TAG,"siteList user: " +user.getFavoriteSite().size());
            if (user.getAvatar() != null) {
                setHeader(user.getDisplayName(), user.getEmail(), new File( user.getAvatar()));
            } else {
                setHeader(user.getDisplayName(), user.getEmail(), R.drawable.avatar_default);
            }


        } else {
            hideItem();
            setHeader("Tài khoản Pega", "Đăng nhập với VietID", R.drawable.logo);
        }
        FragmentMain fragmentMain = new FragmentMain();
        Ultis.tranformFragment(this, R.id.full, fragmentMain);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("MainActivity", "onStart");
        if (SharedPrefs.getInstance().get(Constant.LOGIN, Boolean.class, false)) {
            initUI(true);
        } else {
            hideItem();
            initUI(false);
        }
        if (isChangeInfor) {
            choseFragmentInfor();
            isChangeInfor = false;
        }

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            FragmentMain fragmentMain = new FragmentMain();
            Ultis.tranformFragment(this, R.id.full, fragmentMain);
        }
        if (id == R.id.nav_add_passion) {
            // Handle the camera action
            FragmentChosePassion fragmentChosePassion = new FragmentChosePassion();
            Ultis.tranformFragment(this, R.id.full, fragmentChosePassion);
        } else if (id == R.id.nav_add_site) {
            FragmentChoseSite fragmentChoseSite = new FragmentChoseSite();
            Ultis.tranformFragment(this, R.id.full, fragmentChoseSite);
        } else if (id == R.id.nav_article_saved) {
            FragmentArticleSaved fragmentArticleSaved = new FragmentArticleSaved();
            Ultis.tranformFragment(this,R.id.full,fragmentArticleSaved);
        } else if (id == R.id.nav_infor_user) {
            FragmentInfor fragmentInfor = new FragmentInfor();
            Ultis.tranformFragment(this, R.id.full, fragmentInfor);
        } else if (id == R.id.nav_intro) {

        } else if (id == R.id.nav_logout) {

            if (SharedPrefs.getInstance().get(Constant.LOGIN_FB,Boolean.class,false)) {
                SharedPrefs.getInstance().put(Constant.LOGIN_FB,false);
//                Log.e(TAG,"logout fb");
                disconnectFromFacebook();
//                LoginManager.getInstance().logOut();
            }
            FirebaseAuth.getInstance().signOut();
            SharedPrefs.getInstance().put(Constant.LOGIN, false);
            onStart();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void choseFragmentInfor() {
        FragmentInfor fragmentInfor = new FragmentInfor();
        Ultis.tranformFragment(this, R.id.full, fragmentInfor);
        choseItem(R.id.nav_infor_user);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(TAG,"onActivityResult");
        isChangeInfor = true;
//        choseFragmentInfor();
        super.onActivityResult(requestCode, resultCode, data);
    }
}
