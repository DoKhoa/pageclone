package com.example.nagat.pageclone.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.example.nagat.pageclone.BaseFragment;
import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.model.User;
import com.example.nagat.pageclone.realm.RealmController;
import com.example.nagat.pageclone.ui.MainActivity;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.example.nagat.pageclone.ultis.Ultis;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

/**
 * Created by nagat on 11/5/2018.
 */

public class FragmentInfor extends BaseFragment {

    private static final String TAG = "FragmentInfor";
    CircleImageView img_avatar_infor;
    TextView tv_name_infor_large;
    EditText tv_name_infor;
    TextView tv_email;
    Button btn_edit_name_display;
    LinearLayout ln_change_password;
    Realm realm;
    User user;
    Boolean isEdit = false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_infor, container, false);
        realm = RealmController.with(this).getRealm();
        img_avatar_infor = view.findViewById(R.id.img_avatar_infor);
        img_avatar_infor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAvatar();
            }
        });
        tv_name_infor_large = view.findViewById(R.id.tv_name_infor_large);
        tv_name_infor = view.findViewById(R.id.tv_name_infor);
        tv_email = view.findViewById(R.id.tv_email);
        ln_change_password = view.findViewById(R.id.ln_change_password);

        if (SharedPrefs.getInstance().get(Constant.LOGIN_FB,Boolean.class,false)) {
            ln_change_password.setVisibility(View.GONE);
        } else {
            ln_change_password.setVisibility(View.VISIBLE);
        }
        ln_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentChangePassword fragmentChangePassword = new FragmentChangePassword();
                Ultis.tranformFragment(getContext(),R.id.drawer_layout,fragmentChangePassword);
            }
        });

        btn_edit_name_display = view.findViewById(R.id.btn_edit_name_display);
        btn_edit_name_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEdit) {
                    tv_name_infor.setEnabled(true);
                    isEdit = true;
                } else {
                    if (tv_name_infor.getText().toString().length() < 1) {
                        tv_name_infor.setError("Điền tên muốn sửa");
                    } else {
                        realm.beginTransaction();
                        User user = RealmController.with(FragmentInfor.this).getUser(Ultis.getEmailUser());
                        user.setDisplayName(tv_name_infor.getText().toString());
                        realm.commitTransaction();
                        tv_name_infor_large.setText(tv_name_infor.getText().toString());
                        isEdit = false;
                        tv_name_infor.setEnabled(false);
                        ((MainActivity) getActivity()).setHeader(tv_name_infor.getText().toString(), tv_email.getText().toString());
                    }


                }

            }
        });

        user = new User();
        realm.beginTransaction();
        user = realm.copyFromRealm(RealmController.with(this).getUser(Ultis.getEmailUser()));
        realm.commitTransaction();
        setupUI();
        return view;
    }

    private void changeAvatar() {
        ImagePicker.create(this)
                .returnMode(ReturnMode.ALL) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                .folderMode(true) // set folder mode (false by default)
                .single()
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select")
                .start(0); // image selection title
    }

    public void saveAvatarUser(String avatar) {
        realm.beginTransaction();
        User user = RealmController.with(FragmentInfor.this).getUser(Ultis.getEmailUser());
        user.setAvatar(avatar);
        realm.commitTransaction();
        ((MainActivity) getActivity()).setHeader(tv_name_infor.getText().toString(), tv_email.getText().toString(), new File(avatar));
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        List<Image> images = ImagePicker.getImages(data);
        if (images != null && !images.isEmpty()) {
//            Log.e(TAG,images.get(0).getPath());
            saveAvatarUser(images.get(0).getPath());
            Picasso.get()
                    .load(new File(images.get(0).getPath()))
                    .placeholder(R.drawable.logo)
                    .resize(200,200)
                    .centerCrop()
                    .error(R.drawable.avatar_default)
                    .into(img_avatar_infor);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onStart() {
        super.onStart();
    }


    private void setupUI() {
        tv_name_infor_large.setText(user.getDisplayName());
        tv_name_infor.setText(user.getDisplayName());
        tv_email.setText(user.getEmail());
        if (user.getAvatar()!=null) {
            Picasso.get()
                    .load(new File(user.getAvatar()))
                    .placeholder(R.drawable.logo)
                    .resize(200,200)
                    .centerCrop()
                    .error(R.drawable.avatar_default)
                    .into(img_avatar_infor);
        }
    }
}
