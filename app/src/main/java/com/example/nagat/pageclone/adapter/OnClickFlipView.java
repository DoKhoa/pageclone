package com.example.nagat.pageclone.adapter;

import com.example.nagat.pageclone.model.Passion;
import com.example.nagat.pageclone.model.Site;

import java.util.List;

/**
 * Created by nagat on 2/5/2018.
 */

public interface OnClickFlipView {
    void onClickFlipView(int buttonClick);
    void onClickDone(List<Site> mListSite, List<Passion> mListPassion);
    void onClickGridView(int type,int position,boolean isChose,Object data);
}
