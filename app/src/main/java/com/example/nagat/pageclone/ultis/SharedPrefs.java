package com.example.nagat.pageclone.ultis;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.nagat.pageclone.App;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by nagat on 3/5/2018.
 */

public class SharedPrefs {
    private static final String PREF_NAME = "share_pref";
    private static SharedPrefs mInstance;
    private SharedPreferences mSharedPreferences;

    private SharedPrefs() {
        mSharedPreferences = App.self().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }
    public static SharedPrefs getInstance() {
        if (mInstance==null) {
            mInstance = new SharedPrefs();
        }
        return mInstance;
    }
    public <T> void put(String key, T data) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        if (data instanceof String) {
            editor.putString(key, (String) data);
        } else if (data instanceof Boolean) {
            editor.putBoolean(key, (Boolean) data);
        } else if (data instanceof Float) {
            editor.putFloat(key, (Float) data);
        } else if (data instanceof Integer) {
            editor.putInt(key, (Integer) data);
        } else if (data instanceof Long) {
            editor.putLong(key, (Long) data);
        } else {
            editor.putString(key, App.self().getGSon().toJson(data));
        }
        editor.apply();
    }
    @SuppressWarnings("unchecked")
    public <T> T get(String key, Class<T> anonymousClass) {
        if (anonymousClass == String.class) {
            return (T) mSharedPreferences.getString(key, "");
        } else if (anonymousClass == Boolean.class) {
            return (T) Boolean.valueOf(mSharedPreferences.getBoolean(key, false));
        } else if (anonymousClass == Float.class) {
            return (T) Float.valueOf(mSharedPreferences.getFloat(key, 0));
        } else if (anonymousClass == Integer.class) {
            return (T) Integer.valueOf(mSharedPreferences.getInt(key, 0));
        } else if (anonymousClass == Long.class) {
            return (T) Long.valueOf(mSharedPreferences.getLong(key, 0));
        } else {
            return (T) App.self().getGSon().fromJson(mSharedPreferences.getString(key, ""), anonymousClass);
        }
    }
    public <T> ArrayList<T> getArrayList(String key,Class<T> anonymousClass) {
        Set<String> arr = mSharedPreferences.getStringSet(key,null);
        if (arr == null) {
            return null;
        }
        ArrayList<T> t = new ArrayList<>();
        for (String element: arr) {
            T object =  App.self().getGSon().fromJson(element, anonymousClass);
            t.add(object);
        }

        return t;
    }
    public <T> void  putArrayList(String key, List<T> data) {
        Set<String> set = new HashSet<String>();
        ArrayList<String> listString = new ArrayList<>();
        for (T element : data) {
            String object = App.self().getGSon().toJson(element);
            listString.add(object);
        }
        set.addAll(listString);
        Log.e("khoado","Json: "+set.toString());
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putStringSet(key,set);
        editor.apply();
    }
    @SuppressWarnings("unchecked")
    public <T> T get(String key, Class<T> anonymousClass, T defaultValue) {
        if (anonymousClass == String.class) {
            return (T) mSharedPreferences.getString(key, (String) defaultValue);
        } else if (anonymousClass == Boolean.class) {
            return (T) Boolean.valueOf(mSharedPreferences.getBoolean(key, (Boolean) defaultValue));
        } else if (anonymousClass == Float.class) {
            return (T) Float.valueOf(mSharedPreferences.getFloat(key, (Float) defaultValue));
        } else if (anonymousClass == Integer.class) {
            return (T) Integer.valueOf(mSharedPreferences.getInt(key, (Integer) defaultValue));
        } else if (anonymousClass == Long.class) {
            return (T) Long.valueOf(mSharedPreferences.getLong(key, (Long) defaultValue));
        } else {
            return (T) App.self()
                    .getGSon()
                    .fromJson(mSharedPreferences.getString(key, ""), anonymousClass);
        }
    }

}
