package com.example.nagat.pageclone.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.model.ModelRealm.ArticleRealm;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by nagat on 12/5/2018.
 */

public class ArticleSavedRecyclerViewAdapter extends RecyclerView.Adapter<ArticleSavedRecyclerViewAdapter.RecyclerViewHolder> {
    Context context;
    List<ArticleRealm> list;
    OnClickItemArticleSaved onClickItemArticleSaved;
    public void setOnClickItemArticleSaved(OnClickItemArticleSaved onClickItemArticleSaved) {
        this.onClickItemArticleSaved = onClickItemArticleSaved;
    }
    public interface OnClickItemArticleSaved {
        void onClick(ArticleRealm item);
    }
    public ArticleSavedRecyclerViewAdapter(Context context, List<ArticleRealm> list){
        this.context = context;
        this.list = list;
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_article_saved, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        final ArticleRealm articleRealm = list.get(position);
        holder.tv_title.setText(articleRealm.getTitle());
        holder.tv_desciption.setText(articleRealm.getDescription());
        Picasso.get()
                .load(articleRealm.getImage())
                .placeholder(R.drawable.ic_white_2x)
                .error(R.drawable.ic_white_2x)
                .into(holder.iv_thumbnail);
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickItemArticleSaved.onClick(articleRealm);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_thumbnail;
        TextView tv_title;
        TextView tv_desciption;
        View view;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            iv_thumbnail = itemView.findViewById(R.id.iv_thumbnail);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_desciption = itemView.findViewById(R.id.tv_desciption);
            view = itemView;
        }
    }
}
