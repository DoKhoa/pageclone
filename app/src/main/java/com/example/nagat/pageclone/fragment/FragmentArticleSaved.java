package com.example.nagat.pageclone.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nagat.pageclone.BaseFragment;
import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.adapter.ArticleSavedRecyclerViewAdapter;
import com.example.nagat.pageclone.model.Article;
import com.example.nagat.pageclone.model.ModelRealm.ArticleRealm;
import com.example.nagat.pageclone.realm.RealmController;
import com.example.nagat.pageclone.ui.ReviewNewsActivity;
import com.example.nagat.pageclone.ultis.Ultis;

import java.util.List;

import io.realm.Realm;

/**
 * Created by nagat on 12/5/2018.
 */

public class FragmentArticleSaved extends BaseFragment {
    private static final String TAG = "FragmentArticleSaved";
    RecyclerView recyclerView;
    ArticleSavedRecyclerViewAdapter adapter;
    Realm realm;
    private List<ArticleRealm> listFavoriteArticle;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artucke_saved,container,false);
        recyclerView = view.findViewById(R.id.review);
        realm = RealmController.with(this).getRealm();
        getListFavoriteArticle();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getListFavoriteArticle();
        adapter = new ArticleSavedRecyclerViewAdapter(getContext(),listFavoriteArticle);
        adapter.setOnClickItemArticleSaved(new ArticleSavedRecyclerViewAdapter.OnClickItemArticleSaved() {
            @Override
            public void onClick(ArticleRealm item) {
                Article article = new Article();
                article.setTitle(item.getTitle());
                article.setAuthor(item.getAuthor());
                article.setPubDate(item.getPubDate());
                article.setContent(item.getContent());
                article.setDescription(item.getDescription());
                article.setImage(item.getImage());
                article.setLink(item.getLink());
                FragmentReadNews fragmentReadNews = new FragmentReadNews(article,FragmentArticleSaved.this);
                Ultis.tranformFragment(getContext(),R.id.drawer_layout,fragmentReadNews);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG,"onresume");
    }


    private void getListFavoriteArticle() {
        realm.beginTransaction();
        listFavoriteArticle = realm.copyFromRealm(RealmController.with(this).getUser(Ultis.getEmailUser()).getSavedArticle());
        realm.commitTransaction();
//        Log.e(TAG,"size list favorite: "+ listFavoriteArticle.size());
    }
}
