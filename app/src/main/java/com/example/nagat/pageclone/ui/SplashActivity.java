package com.example.nagat.pageclone.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.Window;
import android.view.WindowManager;

import com.example.nagat.pageclone.BaseActivity;
import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.realm.RealmController;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.DummyData;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.example.nagat.pageclone.ultis.Ultis;
import com.google.firebase.auth.FirebaseAuth;

import io.realm.Realm;

/**
 * Created by nagat on 2/5/2018.
 */

public class SplashActivity extends BaseActivity {
    boolean firstTime = true;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_activity);
        if (FirebaseAuth.getInstance().getCurrentUser()!=null) {
            SharedPrefs.getInstance().put(Constant.LOGIN,true);
        } else {
            SharedPrefs.getInstance().put(Constant.LOGIN,false);
        }

        if (SharedPrefs.getInstance().get(Constant.FIRST_TIME_RUN,Boolean.class,true)) {
            firstTime = true;
        } else {
            firstTime = false;
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (firstTime) {
                    Ultis.initDummyData(SplashActivity.this);
                    Intent intent = new Intent(SplashActivity.this,StartActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        },2000);
    }
}
