package com.example.nagat.pageclone.model.ModelRealm;

import com.example.nagat.pageclone.model.Site;

import java.util.HashMap;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nagat on 11/5/2018.
 */

public class SiteRealm extends RealmObject {
    private String siteName;
    private String avatarSite;
    private String linkRSS;
    @PrimaryKey
    public String id;
    private RealmList<MyMap> passionProvider;

    public SiteRealm() {
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAvatarSite() {
        return avatarSite;
    }

    public void setAvatarSite(String avatarSite) {
        this.avatarSite = avatarSite;
    }

    public String getLinkRSS() {
        return linkRSS;
    }

    public void setLinkRSS(String linkRSS) {
        this.linkRSS = linkRSS;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RealmList<MyMap> getPassionProvider() {
        return passionProvider;
    }

    public void setPassionProvider(RealmList<MyMap> passionProvider) {
        this.passionProvider = passionProvider;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SiteRealm site = (SiteRealm) o;

        return siteName.equals(site.siteName);
    }

    @Override
    public int hashCode() {
        return siteName.hashCode();
    }
}
