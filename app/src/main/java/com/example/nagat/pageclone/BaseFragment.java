package com.example.nagat.pageclone;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

/**
 * Created by nagat on 2/5/2018.
 */

public class BaseFragment extends Fragment {
    private boolean mIsDetachedFromWindow = false;
    private ProgressDialog mProgressDialog;

    @Override
    public void onStart() {
        super.onStart();
        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    public synchronized void showProgress(String message) {
        if (!TextUtils.isEmpty(message)) {
            mProgressDialog.setMessage(message);
        }


        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }
    public synchronized void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing() && !mIsDetachedFromWindow) {
            mProgressDialog.dismiss();
        }
    }

//    public void setupUI(final View view) {
//        if (!(view instanceof EditText) && !(view instanceof Button) && !(view instanceof ImageButton)) {
//            view.setOnTouchListener(new View.OnTouchListener() {
//                public boolean onTouch(final View view, final MotionEvent motionEvent) {
//                    hideSoftKeyboard(view);
//                    return false;
//                }
//            });
//        }
//        if (view instanceof ViewGroup) {
//            for (int i = 0; i < ((ViewGroup) view).getChildCount(); ++i) {
//                this.setupUI(((ViewGroup) view).getChildAt(i));
//            }
//        }
//    }
//
//    public void hideSoftKeyboard(final View view) {
//        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
//    }
}
