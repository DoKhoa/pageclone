package com.example.nagat.pageclone.network;


import android.util.Log;

import com.example.nagat.pageclone.model.Article;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class XMLParser extends Observable {

    private List<Article> articles;
    private Article currentArticle;
    public XMLParser(List<Article> articles) {
        this.articles = articles;
        currentArticle = new Article();

    }

    public void parseXML(String xml,String nameSite) throws XmlPullParserException, IOException {
        currentArticle.setAuthor(nameSite);
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();

        factory.setNamespaceAware(false);
        XmlPullParser xmlPullParser = factory.newPullParser();

        xmlPullParser.setInput(new StringReader(xml));
        boolean insideItem = false;
        int eventType = xmlPullParser.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {

            if (eventType == XmlPullParser.START_TAG) {


                if (xmlPullParser.getName().equalsIgnoreCase("item")) {

                    insideItem = true;

                } else if (xmlPullParser.getName().equalsIgnoreCase("title")) {

                    if (insideItem) {
                        String title = xmlPullParser.nextText();
                        currentArticle.setTitle(title);
                    }

                } else if (xmlPullParser.getName().equalsIgnoreCase("link")) {

                    if (insideItem) {
                        String link = xmlPullParser.nextText();
                        currentArticle.setLink(link);
                    }

                } else if (xmlPullParser.getName().equalsIgnoreCase("pubDate")) {

                    if (insideItem) {
                        String link = xmlPullParser.nextText();
                        currentArticle.setPubDate(link);
                    }

                } else if (xmlPullParser.getName().equalsIgnoreCase("dc:creator")) {

                    if (insideItem) {
                        String author = xmlPullParser.nextText();
//                        currentArticle.setAuthor(author);
                    }

                } else if (xmlPullParser.getName().equalsIgnoreCase("media:thumbnail")) {

                    if (insideItem) {
                        String img = xmlPullParser.getAttributeValue(null, "url");
                        currentArticle.setImage(img);
                    }

                } else if (xmlPullParser.getName().equalsIgnoreCase("media:content")) {

                    if (insideItem) {
                        String img = xmlPullParser.getAttributeValue(null, "url");
                        currentArticle.setImage(img);
                    }

                } else if (xmlPullParser.getName().equalsIgnoreCase("description")) {

                    if (insideItem) {
                        String description = xmlPullParser.nextText();
                        Document doc = Jsoup.parse(description);
                        String descriptionText = doc.text();
                        if (currentArticle.getImage() == null) {
                            try {
                                String pic = doc.select("img").first().attr("abs:src");
                                currentArticle.setImage(pic);
                            } catch (NullPointerException e) {
                                currentArticle.setImage(null);
                            }
                        }
                        currentArticle.setDescription(descriptionText);
                    }

                } else if (xmlPullParser.getName().equalsIgnoreCase("content:encoded")) {

                    if (insideItem) {
                        String htmlData = xmlPullParser.nextText();
                        if (currentArticle.getImage() == null) {
                            Document doc = Jsoup.parse(htmlData);
                            try {
                                String pic = doc.select("img").first().attr("abs:src");
                                currentArticle.setImage(pic);
                            } catch (NullPointerException e) {
                                currentArticle.setImage(null);
                            }
                        }
                        currentArticle.setContent(htmlData);
                    }

                }

            } else if (eventType == XmlPullParser.END_TAG && xmlPullParser.getName().equalsIgnoreCase("item")) {
                insideItem = false;
                articles.add(currentArticle);
                currentArticle = new Article();
                currentArticle.setAuthor(nameSite);

            }
            eventType = xmlPullParser.next();
        }
        triggerObserver();
    }


    private void triggerObserver() {
        setChanged();
//        Log.e("khoado.triggerObserver","articles: "+articles.size());
        notifyObservers(articles);
    }
}
