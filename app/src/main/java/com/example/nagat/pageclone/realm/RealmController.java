package com.example.nagat.pageclone.realm;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import com.example.nagat.pageclone.BaseActivity;
import com.example.nagat.pageclone.BaseFragment;
import com.example.nagat.pageclone.model.ModelRealm.SiteRealm;
import com.example.nagat.pageclone.model.Site;
import com.example.nagat.pageclone.model.User;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by nagat on 11/5/2018.
 */

public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(BaseFragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(BaseActivity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }


    //find all objects in the Book.class
    public RealmResults<User> getAllUser() {
        return realm.where(User.class).findAll();
    }
    public RealmResults<SiteRealm> getAllSite() {
        return realm.where(SiteRealm.class).findAll();
    }
    //query a single item with the given id
    public User getUser(String email) {

        return realm.where(User.class).equalTo("email", email).findFirst();
    }

    //check if Book.class is empty
    public boolean hasUser() {

        return !realm.where(User.class).findAll().isEmpty();
    }

//    query example
//    public RealmResults<Book> queryedBooks() {
//
//        return realm.where(Book.class)
//                .contains("author", "Author 0")
//                .or()
//                .contains("title", "Realm")
//                .findAll();
//
//    }
}
