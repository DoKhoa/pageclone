package com.example.nagat.pageclone.network;

import android.os.AsyncTask;
import android.util.Log;


import com.example.nagat.pageclone.model.Article;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.security.auth.login.LoginException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ParserTask extends AsyncTask<String, Void, String> implements Observer {

    private XMLParser xmlParser;
    private List<Article> articles;
    private String nameSite;
    private OnTaskCompleted onComplete;

    public ParserTask(List<Article> articles,String nameSite) {
        this.articles = articles;
        xmlParser = new XMLParser(this.articles);
        xmlParser.addObserver(this);
        this.nameSite = nameSite;
    }

    public interface OnTaskCompleted {
        void onTaskCompleted(List<Article> list);

        void onError(String error);
        void onPreExecute();
    }

    public void setOnComplete(OnTaskCompleted onComplete) {
        this.onComplete = onComplete;
    }

    @Override
    protected void onPreExecute() {
        onComplete.onPreExecute();

    }

    @Override
    protected String doInBackground(String... ulr) {

        Response response;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(ulr[0])
                .build();
//        Log.e("parserTask.doinback",ulr[0]);
        try {
            response = client.newCall(request).execute();
            if (response.isSuccessful())
                return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
            onComplete.onError(e.toString());
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {

        if (result != null) {
            try {
                xmlParser.parseXML(result,nameSite);
                Log.i("RSS ParserTask ", "RSS parsed correctly!");
            } catch (Exception e) {
                e.printStackTrace();
                onComplete.onError(e.toString());
            }
        } else
            onComplete.onError("result is null");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Observable observable, Object data) {
        articles = (ArrayList<Article>) data;
//        Log.e("khoado.update",((ArrayList<Article>) data).size()+"");
        onComplete.onTaskCompleted(articles);
    }

}
