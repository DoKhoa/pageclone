package com.example.nagat.pageclone.adapter;

import com.example.nagat.pageclone.model.Article;
import com.example.nagat.pageclone.model.ModelRealm.ArticleRealm;
import com.example.nagat.pageclone.model.Passion;
import com.example.nagat.pageclone.model.model_rss.Item;

/**
 * Created by nagat on 8/5/2018.
 */

public interface OnClick {
    void onClickView(Passion passion);
    void onClickReviewNews(Article item);
    void onClickFavorite(ArticleRealm item, Boolean isFavorite);
}
