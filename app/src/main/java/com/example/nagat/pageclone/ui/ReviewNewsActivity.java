package com.example.nagat.pageclone.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.nagat.pageclone.BaseActivity;
import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.adapter.FlipViewReviewNewsAdapter;
import com.example.nagat.pageclone.adapter.OnClick;
import com.example.nagat.pageclone.fragment.FragmentChosePassion;
import com.example.nagat.pageclone.fragment.FragmentReadNews;
import com.example.nagat.pageclone.model.Article;
import com.example.nagat.pageclone.model.ModelRealm.ArticleRealm;
import com.example.nagat.pageclone.model.ModelRealm.SiteRealm;
import com.example.nagat.pageclone.model.Passion;
import com.example.nagat.pageclone.model.Site;
import com.example.nagat.pageclone.model.User;
import com.example.nagat.pageclone.network.ParserTask;
import com.example.nagat.pageclone.realm.RealmController;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.example.nagat.pageclone.ultis.Ultis;
import com.facebook.share.Share;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import se.emilsjolander.flipview.FlipView;

/**
 * Created by nagat on 8/5/2018.
 */

public class ReviewNewsActivity  extends BaseActivity{
    private static final String TAG = "ReviewNewsActivity";
    private Passion passion;
    private ImageView iv_back,upWard;
    private TextView title;
    private FlipView flip_view;
    private List<Site> mListSiteChose;
    private ProgressBar progressBar;
    private List<Article> items;
    private List<ArticleRealm> listFavoriteArticle;
    FlipViewReviewNewsAdapter flipViewReviewNewsAdapter;
    Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_news);
        realm = RealmController.with(this).getRealm();
        if (getIntent().getExtras().get(Constant.PASSION)!=null) {
            passion = (Passion) getIntent().getExtras().get(Constant.PASSION);
        }
        iv_back = findViewById(R.id.iv_back);
        title = findViewById(R.id.tv_title);
        flip_view = findViewById(R.id.flip_view);
        progressBar = findViewById(R.id.progress_bar);
        upWard = findViewById(R.id.upWard);
        upWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flip_view.smoothFlipTo(0);
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        title.setText(passion.getNamePaper());
        getListFavoriteArticle();
        preData();
//        Log.e("khoado",items.size()+"");
        flipViewReviewNewsAdapter = new FlipViewReviewNewsAdapter(this,items,listFavoriteArticle);
        flipViewReviewNewsAdapter.setOnClick(new OnClick() {
            @Override
            public void onClickView(Passion passion) {

            }

            @Override
            public void onClickReviewNews(Article item) {
                FragmentReadNews fragmentReadNews = new FragmentReadNews(item);
                Ultis.tranformFragment(ReviewNewsActivity.this,R.id.full,fragmentReadNews);
            }

            @Override
            public void onClickFavorite(ArticleRealm item, Boolean isFavorite) {
                if (isFavorite) {
                    RealmList<ArticleRealm> realmList = new RealmList<>();
                    User user = RealmController.with(ReviewNewsActivity.this).getUser(Ultis.getEmailUser());
                    if (user.getSavedArticle()!= null) {
                        realmList = user.getSavedArticle();
                    }
                    listFavoriteArticle.add(item);
                    realm.beginTransaction();
                    realmList.add(item);
                    realm.commitTransaction();
//                    Log.e(TAG,"size article favorite: "+realmList.size());
                } else {
                    RealmList<ArticleRealm> realmList = new RealmList<>();
                    User user = RealmController.with(ReviewNewsActivity.this).getUser(Ultis.getEmailUser());
                    if (user.getSavedArticle()!= null) {
                        realmList = user.getSavedArticle();
                    }
                    for (int i=0;i<listFavoriteArticle.size();i++) {
                        ArticleRealm articleRealm = listFavoriteArticle.get(i);
                        if (item.getLink().equals(articleRealm.getLink())) {
                            listFavoriteArticle.remove(articleRealm);
                        }
                    }
                    realm.beginTransaction();
                    realmList.where().equalTo("link",item.getLink()).findFirst().deleteFromRealm();
                    realm.commitTransaction();
//                    Log.e(TAG,"size article favorite: "+realmList.size());
                }
//                getListFavoriteArticle();
//                Log.e(TAG,"size article favorite in listFavoriteArticle: "+listFavoriteArticle.size());
                flipViewReviewNewsAdapter.notifyDataSetChanged();
            }

        });
        flip_view.setAdapter(flipViewReviewNewsAdapter);
    }
    public void notifyChanged() {
        getListFavoriteArticle();
        flipViewReviewNewsAdapter.setListFavorite(listFavoriteArticle);
        Log.e(TAG,"notifychanged size list favorite: " + listFavoriteArticle.size());
        flipViewReviewNewsAdapter.notifyDataSetChanged();
    }
    private void getListFavoriteArticle() {
        if (!SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false) ){
            listFavoriteArticle = new ArrayList<>();
        } else {
            realm.beginTransaction();
            listFavoriteArticle = realm.copyFromRealm(RealmController.with(this).getUser(Ultis.getEmailUser()).getSavedArticle());
            realm.commitTransaction();
        }

//        Log.e(TAG,"size list favorite: "+ listFavoriteArticle.size());
    }
    private void preData() {
        items = new ArrayList<>();
        if (!SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false)) {
            if (SharedPrefs.getInstance().getArrayList(Constant.LIST_PASSION_CHOSE,Site.class)!=null) {
                mListSiteChose = SharedPrefs.getInstance().getArrayList(Constant.LIST_SITE_CHOSE,Site.class);
            }
        } else {
            List<SiteRealm> realmRealmList;
            realm.beginTransaction();
            realmRealmList = realm.copyFromRealm(RealmController.with(this).getUser(Ultis.getEmailUser()).getFavoriteSite());
            realm.commitTransaction();
            mListSiteChose = Ultis.convertRealmListToListSite(realmRealmList);
        }
        for (Site site : mListSiteChose) {
            if (site.getPassionProvider().containsKey(passion.getId())) {
                ParserTask parserTask = new ParserTask(items,site.getSiteName());
                parserTask.setOnComplete(new ParserTask.OnTaskCompleted() {
                    @Override
                    public void onTaskCompleted(List<Article> list) {
                        progressBar.setVisibility(View.GONE);
                        flip_view.setVisibility(View.VISIBLE);
                        upWard.setVisibility(View.VISIBLE);
                        items = list;
                        flipViewReviewNewsAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(String error) {
                        Log.e("khoado","error:"+ error);
                        Ultis.showDialog(ReviewNewsActivity.this, "Lỗi", "Lỗi load news", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        },R.drawable.ic_error_black_24dp);
                    }

                    @Override
                    public void onPreExecute() {
                        progressBar.setVisibility(View.VISIBLE);
                        flip_view.setVisibility(View.GONE);
                        upWard.setVisibility(View.GONE);
                    }
                });
                parserTask.execute(site.getPassionProvider().get(passion.getId()));
            }
        }
    }
}
