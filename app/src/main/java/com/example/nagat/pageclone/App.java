package com.example.nagat.pageclone;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.google.gson.Gson;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by nagat on 3/5/2018.
 */

public class App extends Application {
    private static App mSelf;
    private Gson mGSon;
    public static App self() {
        return mSelf;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);
        FacebookSdk.sdkInitialize(getApplicationContext());
        mSelf =this;
        mGSon = new Gson();
    }
    public Gson getGSon() {
        return mGSon;
    }
}
