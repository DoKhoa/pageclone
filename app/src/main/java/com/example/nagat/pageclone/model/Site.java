package com.example.nagat.pageclone.model;

import java.util.HashMap;

/**
 * Created by nagat on 2/5/2018.
 */

public class Site {
    private String siteName;
    private String avatarSite;
    private String linkRSS;
    public String id;
    private HashMap<String,String> passionProvider;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Site() {
    }

    public Site(String siteName, String avatarSite, String id) {
        this.siteName = siteName;
        this.avatarSite = avatarSite;
        this.id = id;
    }

    public Site(String siteName, String avatarSite, String id, HashMap<String, String> passionProvider) {
        this.siteName = siteName;
        this.avatarSite = avatarSite;
        this.id = id;
        this.passionProvider = passionProvider;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAvatarSite() {
        return avatarSite;
    }

    public void setAvatarSite(String avatarSite) {
        this.avatarSite = avatarSite;
    }

    public String getLinkRSS() {
        return linkRSS;
    }

    public void setLinkRSS(String linkRSS) {
        this.linkRSS = linkRSS;
    }

    public HashMap<String, String> getPassionProvider() {
        return passionProvider;
    }

    public void setPassionProvider(HashMap<String, String> passionProvider) {
        this.passionProvider = passionProvider;
    }
    public void addPassionProvider(String key,String value) {
        this.passionProvider.put(key,value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Site site = (Site) o;

        return siteName.equals(site.siteName);
    }

    @Override
    public int hashCode() {
        return siteName.hashCode();
    }
}
