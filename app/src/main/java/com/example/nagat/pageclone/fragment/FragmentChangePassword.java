package com.example.nagat.pageclone.fragment;

import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.nagat.pageclone.BaseActivity;
import com.example.nagat.pageclone.BaseFragment;
import com.example.nagat.pageclone.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by nagat on 7/5/2018.
 */

public class FragmentChangePassword extends BaseFragment {

    EditText old_password;
    EditText new_password;
    EditText repeat_new_passwrod;
    Button change_password;
    ImageView iv_back;
    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_password,container,false);
        old_password = view.findViewById(R.id.old_password);
        new_password = view.findViewById(R.id.new_password);
        repeat_new_passwrod = view.findViewById(R.id.repeat_new_password);
        iv_back = view.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).hideSoftKeyboard();
                getFragmentManager().popBackStack();
            }
        });
        change_password = view.findViewById(R.id.change_password);
        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    showProgress(null);
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    AuthCredential credential = EmailAuthProvider
                            .getCredential(user.getEmail(), old_password.getText().toString());
                    user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                FirebaseAuth.getInstance().getCurrentUser().updatePassword(new_password.getText().toString())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Toast.makeText(getActivity(), "Thay đổi mật khẩu thành công", Toast.LENGTH_SHORT).show();
                                                    hideProgress();
                                                    ((BaseActivity) getActivity()).hideSoftKeyboard();
                                                    getFragmentManager().popBackStack();
                                                }
                                            }
                                        });
                            } else {
                                hideProgress();
                                Toast.makeText(getActivity(), "Mật khẩu cũ không đúng", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });

                }

            }

        });
        return view;
    }

    private boolean validate() {


        if (new_password.getText().toString().trim().isEmpty()) {
            new_password.setError("Mật khẩu mới ko thể trống");
            return false;
        }

        if (!new_password.getText().toString().equals(repeat_new_passwrod.getText().toString())) {
            repeat_new_passwrod.setError("Mật khẩu chưa trùng khớp");
            return false;
        }
        return true;
    }
}
