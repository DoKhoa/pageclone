package com.example.nagat.pageclone.model.ModelRealm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nagat on 11/5/2018.
 */

public class MyMap extends RealmObject {
    @PrimaryKey
    private String key;
    private String value;

    public MyMap() {
    }

    public MyMap(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyMap myMap = (MyMap) o;

        return key != null ? key.equals(myMap.key) : myMap.key == null;
    }

    @Override
    public int hashCode() {
        return key != null ? key.hashCode() : 0;
    }
}
