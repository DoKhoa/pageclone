package com.example.nagat.pageclone.model.model_rss;

import java.util.List;

/**
 * Created by nagat on 8/5/2018.
 */

public class ModelRss {
    private String status;
    private Feed feed;
    private List<Item> itemList;

    public ModelRss() {
    }

    public ModelRss(String status, Feed feed, List<Item> itemList) {
        this.status = status;
        this.feed = feed;
        this.itemList = itemList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }
}
