package com.example.nagat.pageclone.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nagat.pageclone.BaseActivity;
import com.example.nagat.pageclone.BaseFragment;
import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.model.User;
import com.example.nagat.pageclone.realm.RealmController;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.ProviderQueryResult;

import io.realm.Realm;

/**
 * Created by nagat on 2/5/2018.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "LoginActivity";
    private EditText et_email;
    private EditText et_password;
    private TextView tvDangNhap;
    private TextView tvDangKy;
    private TextView forgotPassword;
    private FirebaseAuth auth;
    LoginButton login_button;
    Realm realm;
    CallbackManager mCallbackManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.setupUI(this.getWindow().getDecorView().findViewById(android.R.id.content));
        auth = FirebaseAuth.getInstance();
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        tvDangNhap = findViewById(R.id.tvDangNhap);
        tvDangKy = findViewById(R.id.tvDangKy);
        forgotPassword = findViewById(R.id.forgotPassword);
        login_button = findViewById(R.id.login_button);
        tvDangKy.setOnClickListener(this);
        tvDangNhap.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);
        realm = RealmController.with(this).getRealm();
        mCallbackManager = CallbackManager.Factory.create();
        login_button.setReadPermissions("email", "public_profile", "user_friends");
        login_button.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                // ...
            }
        });
    }
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        showProgress();
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            SharedPrefs.getInstance().put(Constant.LOGIN, true);
                            SharedPrefs.getInstance().put(Constant.LOGIN_FB, true);
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = auth.getCurrentUser();
                            Log.d(TAG, user.getEmail());
                            Log.d(TAG, user.getDisplayName());
                            addUserFacebook(user);
                            sendBack();
//                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Lỗi đăng nhập facebook",
                                    Toast.LENGTH_SHORT).show();
//                            updateUI(null);
                        }

                        // ...
                    }
                });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private boolean checkAccountEmailExist(String email) {
        return RealmController.with(this).getUser(email) == null ? false : true;
    }
    private void addUserFacebook(FirebaseUser user1) {
//        Log.e(TAG+".addUserFacebook",checkAccountEmailExist(user1.getEmail())+"");
//        Log.e(TAG+".addUserFacebook",checkAccountEmailExist("abc")+"");
        if (!checkAccountEmailExist(user1.getEmail())) {
//            Log.e(TAG+".addUserFacebook",!checkAccountEmailExistInFirebase(user1.getEmail())+"");
            User user = new User();
            user.setEmail(user1.getEmail());
            user.setDisplayName(user1.getDisplayName());
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(user);
            realm.commitTransaction();
        }
    }
    private void addUser() {
        User user = new User();
        user.setEmail(et_email.getText().toString());
        user.setDisplayName(et_email.getText().toString());
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvDangNhap) {
            Log.e(TAG, "click dang nhap");
            if (validate()) {
                showProgress();
                auth.signInWithEmailAndPassword(et_email.getText().toString(), et_password.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    SharedPrefs.getInstance().put(Constant.LOGIN, true);
                                    Toast.makeText(LoginActivity.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                                    hideProgress();
//                                    addUser();
                                    sendBack();
                                } else {
                                    Toast.makeText(LoginActivity.this, "Lỗi đăng nhập", Toast.LENGTH_SHORT).show();
                                    hideProgress();
                                }
                            }
                        });
            }
        } else if (v.getId() == R.id.tvDangKy) {
            Log.e(TAG, "click dang ky");
            if (validate()) {
                showProgress();
                auth.createUserWithEmailAndPassword(et_email.getText().toString(), et_password.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    SharedPrefs.getInstance().put(Constant.LOGIN, true);
                                    addUser();
                                    hideProgress();
                                    Toast.makeText(LoginActivity.this, "Đăng ký thành công", Toast.LENGTH_SHORT).show();
                                    sendBack();
                                } else {
                                    Toast.makeText(LoginActivity.this, "Lỗi đăng ký", Toast.LENGTH_SHORT).show();
                                    hideProgress();
                                }
                            }
                        });
            }
        } else if (v.getId() == R.id.forgotPassword) {
            Log.e(TAG, "click quen mat khau");
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Quên mật khẩu");
            alert.setMessage("Nhập email:");
            // Set an EditText view to get user input
            final EditText input = new EditText(this);
            alert.setView(input);
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int whichButton) {
                    showProgress();
                    String value = input.getText().toString();
                    auth.sendPasswordResetEmail(value)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(LoginActivity.this, "Reset password email is sent!", Toast.LENGTH_SHORT).show();
                                        hideProgress();
                                    } else {
                                        Toast.makeText(LoginActivity.this, "Failed to send reset email!", Toast.LENGTH_SHORT).show();
                                        hideProgress();
                                    }
                                }
                            });
                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.
                }
            });

            alert.show();
        }
//        else if (v.getId() == R.id.logo_fb) {
//            Log.e(TAG, "click logo fb");
//        }
    }
    private void sendBack() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
    private boolean validate() {
        if (et_email.getText().toString().isEmpty() || !et_email.getText().toString().contains("@gmail.com")) {
            et_email.setError("email phải có dạng abc@gmail.com");
            return false;
        }
        if (et_password.getText().toString().length() < 6) {
            et_password.setError("mật khẩu phải lớn hơn 6 ký tự");
            return false;
        }
        return true;
    }
}
