package com.example.nagat.pageclone.network;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.example.nagat.pageclone.model.Article;
import com.example.nagat.pageclone.model.model_rss.Feed;
import com.example.nagat.pageclone.model.model_rss.Item;
import com.example.nagat.pageclone.model.model_rss.ModelRss;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nagat on 8/5/2018.
 */

public class DataAccessObject {
    public static void loadRSS(String url, final OnLoadRss onLoadRss) {
        @SuppressLint("StaticFieldLeak") AsyncTask<String, String, String> loadRSSTask = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                onLoadRss.onPreExecute();
            }

            @Override
            protected String doInBackground(String... strings) {
                String result;
                HTTPDataHandler httpDataHandler = new HTTPDataHandler();
                result  = httpDataHandler.getHTTPData(strings[0]);
                return result;
            }

            @Override
            protected void onPostExecute(String s) {
                if (s!=null ) {
                    try {
                        ModelRss modelRss = new ModelRss();
                        JSONObject reader = new JSONObject(s);
                        modelRss.setStatus(reader.getString("status"));
                        JSONObject feed_rss = reader.getJSONObject("feed");
                        Feed feed = new Feed();
                        feed.setTitle(feed_rss.getString("title"));
                        modelRss.setFeed(feed);
                        JSONArray items_rss = reader.getJSONArray("items");
                        List<Item> items = new ArrayList<>();
                        for (int i =0; i<items_rss.length();i++ ) {
                            JSONObject item_rss = items_rss.getJSONObject(i);
                            Item item = new Item();
                            item.setTitle(item_rss.getString("title"));
                            item.setThumbnail(item_rss.getString("thumbnail"));
                            item.setLink(item_rss.getString("link"));
                            item.setPubDate(item_rss.getString("pubDate"));
                            item.setAuthor(feed.getTitle());
                            item.setDescription(item_rss.getString("description"));
                            items.add(item);
                        }
                        modelRss.setItemList(items);
                        onLoadRss.onSuccessLoadRss(modelRss);
                        Log.e("khoado.dataaccesobject",items.size()+"");
                    } catch(JSONException e){
                        e.printStackTrace();
                    }
                }


            }
        };
        loadRSSTask.execute(url);
    }

}
