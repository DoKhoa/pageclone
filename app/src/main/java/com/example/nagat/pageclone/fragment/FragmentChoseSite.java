package com.example.nagat.pageclone.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.example.nagat.pageclone.BaseFragment;
import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.adapter.GrillViewChosePassionAdapter;
import com.example.nagat.pageclone.adapter.GrillViewChoseSiteAdapter;
import com.example.nagat.pageclone.adapter.OnClickFlipView;
import com.example.nagat.pageclone.model.ModelRealm.PassionRealm;
import com.example.nagat.pageclone.model.ModelRealm.SiteRealm;
import com.example.nagat.pageclone.model.Passion;
import com.example.nagat.pageclone.model.Site;
import com.example.nagat.pageclone.model.User;
import com.example.nagat.pageclone.realm.RealmController;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.DummyData;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.example.nagat.pageclone.ultis.Ultis;

import java.util.List;

import io.realm.Realm;
import okhttp3.internal.Util;

/**
 * Created by nagat on 3/5/2018.
 */

public class FragmentChoseSite extends BaseFragment {
    private GridView gridView;
    private List<Site> mListSite;
    private List<Site> mListSiteChose;
    private TextView btDone;
    Realm realm;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chose_site,container,false);
        gridView = view.findViewById(R.id.gridView);
        btDone  = view.findViewById(R.id.tvDone);
        realm = RealmController.with(this).getRealm();
        btDone.setVisibility(View.VISIBLE);
        btDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false)) {
                    User user = RealmController.with(FragmentChoseSite.this).getUser(Ultis.getEmailUser());
                    User user1 = new User();
                    user1.setEmail(user.getEmail());
                    user1.setDisplayName(user.getDisplayName());
                    user1.setFavoriteSite(Ultis.convertListToRealmListSite(mListSiteChose));
                    user1.setFavoritePassion(user.getFavoritePassion());
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(user1);
                    realm.commitTransaction();

                } else {
                    SharedPrefs.getInstance().putArrayList(Constant.LIST_SITE_CHOSE,mListSiteChose);
                }

                final FragmentMain fragmentMain = new FragmentMain();
                Ultis.showDialog(getContext(), "Chỉnh sửa đầu báo", "Hoàn tất chỉnh sửa", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Ultis.tranformFragmentNotAddToStack(getContext(),R.id.full,fragmentMain);
                    }
                },R.drawable.icons8_ok);

            }
        });
        if (!SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false)) {
            if (SharedPrefs.getInstance().getArrayList(Constant.LIST_PASSION_CHOSE,Site.class)!=null) {
                mListSiteChose = SharedPrefs.getInstance().getArrayList(Constant.LIST_SITE_CHOSE,Site.class);
            }
        } else {
            List<SiteRealm> realmRealmList;
            realm.beginTransaction();
            realmRealmList = realm.copyFromRealm(RealmController.with(this).getUser(Ultis.getEmailUser()).getFavoriteSite());
            realm.commitTransaction();
            mListSiteChose = Ultis.convertRealmListToListSite(realmRealmList);
        }
        mListSite = SharedPrefs.getInstance().getArrayList(Constant.LIST_SITE,Site.class);
        GrillViewChoseSiteAdapter adapter = new GrillViewChoseSiteAdapter(getActivity(),mListSite,mListSiteChose);
        adapter.setOnClickFlipView(new OnClickFlipView() {
            @Override
            public void onClickFlipView(int buttonClick) {

            }

            @Override
            public void onClickDone(List<Site> mListSite, List<Passion> mListPassion) {

            }

            @Override
            public void onClickGridView(int type, int position, boolean isChose, Object data) {
                if (type == Constant.TYPE_GRIDVIEW_SITE) {
                    if (isChose) {
                        mListSiteChose.add((Site) data);
                    } else {
                        mListSiteChose.remove((Site) data);
                    }
                }

            }
        });
        gridView.setAdapter(adapter);
//        this.setupUI(getActivity().getWindow().getDecorView().findViewById(android.R.id.content));
        return view;
    }
}
