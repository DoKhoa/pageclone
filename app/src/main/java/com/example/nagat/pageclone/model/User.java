package com.example.nagat.pageclone.model;

import com.example.nagat.pageclone.model.ModelRealm.ArticleRealm;
import com.example.nagat.pageclone.model.ModelRealm.PassionRealm;
import com.example.nagat.pageclone.model.ModelRealm.SiteRealm;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nagat on 3/5/2018.
 */

public class User extends RealmObject {
    @PrimaryKey
    private String email;
    private String displayName;
    private String avatar;
    private RealmList<PassionRealm> favoritePassion;
    private RealmList<SiteRealm> favoriteSite;
    private RealmList<ArticleRealm> savedArticle;

    public User() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public RealmList<PassionRealm> getFavoritePassion() {
        return favoritePassion;
    }

    public void setFavoritePassion(RealmList<PassionRealm> favoritePassion) {
        this.favoritePassion = favoritePassion;
    }

    public RealmList<SiteRealm> getFavoriteSite() {
        return favoriteSite;
    }

    public void setFavoriteSite(RealmList<SiteRealm> favoriteSite) {
        this.favoriteSite = favoriteSite;
    }

    public RealmList<ArticleRealm> getSavedArticle() {
        return savedArticle;
    }

    public void setSavedArticle(RealmList<ArticleRealm> savedArticle) {
        this.savedArticle = savedArticle;
    }
}
