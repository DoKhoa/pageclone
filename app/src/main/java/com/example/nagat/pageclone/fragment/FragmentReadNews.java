package com.example.nagat.pageclone.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.nagat.pageclone.BaseFragment;
import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.model.Article;
import com.example.nagat.pageclone.model.ModelRealm.ArticleRealm;
import com.example.nagat.pageclone.model.User;
import com.example.nagat.pageclone.model.model_rss.Item;
import com.example.nagat.pageclone.realm.RealmController;
import com.example.nagat.pageclone.ui.ReviewNewsActivity;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.example.nagat.pageclone.ultis.Ultis;
import com.facebook.share.Share;

import java.util.ArrayList;
import java.util.List;

import im.delight.android.webview.AdvancedWebView;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by nagat on 8/5/2018.
 */

@SuppressLint("ValidFragment")
public class FragmentReadNews extends BaseFragment implements AdvancedWebView.Listener {
    private Article item;
    private ArticleRealm itemRealm;
    private ImageView iv_back;
    private AdvancedWebView advancedWebView;
    private ImageView iv_favorite;
    private ImageView iv_share;
    private FragmentArticleSaved fragmentArticleSaved;
    public FragmentReadNews(Article item) {
        this.item = item;
    }
    public FragmentReadNews(Article item,FragmentArticleSaved fragmentArticleSaved) {
        this.item = item;
        this.fragmentArticleSaved = fragmentArticleSaved;
    }
    Realm realm;
    private List<ArticleRealm> listFavoriteArticle;
    private boolean isFavorite;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_read_news,container,false);
        realm = RealmController.with(this).getRealm();
        iv_share = view.findViewById(R.id.iv_share);
        iv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBodyText = item.getTitle() + "\nLink: "+item.getLink()+"\nĐọc báo với PegaClone";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Tin tức từ PegaClone");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(sharingIntent, "Sharing With"));
            }
        });
        iv_back = view.findViewById(R.id.iv_back);
        iv_favorite = view.findViewById(R.id.iv_favorite);

        getListFavoriteArticle();
        isFavorite = false;
        for (int i =0; i<listFavoriteArticle.size();i++) {
            ArticleRealm articleRealm = listFavoriteArticle.get(i);
            if (item.getLink().equals(articleRealm.getLink())) {
                iv_favorite.setImageResource(R.drawable.icon_favorite_clicked);
                isFavorite = true;
                itemRealm = articleRealm;
            }
        }
        if (SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false)) {
            iv_favorite.setVisibility(View.VISIBLE);
        } else {
            iv_favorite.setVisibility(View.GONE);
        }
        iv_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFavorite) {
                    isFavorite = false;
                    iv_favorite.setImageResource(R.drawable.icon_favorite);

                    RealmList<ArticleRealm> realmList = new RealmList<>();
                    User user = RealmController.with(FragmentReadNews.this).getUser(Ultis.getEmailUser());
                    if (user.getSavedArticle()!= null) {
                        realmList = user.getSavedArticle();
                    }
                    realm.beginTransaction();
                    realmList.where().equalTo("link",item.getLink()).findFirst().deleteFromRealm();
                    realm.commitTransaction();
                } else {
                    itemRealm = new ArticleRealm();
                    itemRealm.setTitle(item.getTitle());
                    itemRealm.setAuthor(item.getAuthor());
                    itemRealm.setContent(item.getContent());
                    itemRealm.setDescription(item.getDescription());
                    itemRealm.setLink(item.getLink());
                    itemRealm.setImage(item.getImage());
                    itemRealm.setPubDate(item.getPubDate());
                    isFavorite = true;
                    iv_favorite.setImageResource(R.drawable.icon_favorite_clicked);
                    RealmList<ArticleRealm> realmList = new RealmList<>();
                    User user = RealmController.with(FragmentReadNews.this).getUser(Ultis.getEmailUser());
                    if (user.getSavedArticle()!= null) {
                        realmList = user.getSavedArticle();
                    }
                    realm.beginTransaction();
                    realmList.add(itemRealm);
                    realm.commitTransaction();

                }
                if (getActivity() instanceof ReviewNewsActivity) {
                    ((ReviewNewsActivity) getActivity()).notifyChanged();
                }

            }
        });
        advancedWebView = view.findViewById(R.id.web_view);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragmentArticleSaved!=null) {
                    fragmentArticleSaved.onStart();
                }
                getFragmentManager().popBackStack();

            }
        });
        advancedWebView.setListener(getActivity(),this);
        advancedWebView.loadUrl(item.getLink());
        return view;
    }
    private void getListFavoriteArticle() {
        if (!SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false) ){
            listFavoriteArticle = new ArrayList<>();
        } else {
            realm.beginTransaction();
            listFavoriteArticle = realm.copyFromRealm(RealmController.with(this).getUser(Ultis.getEmailUser()).getSavedArticle());
            realm.commitTransaction();
        }
//        Log.e(TAG,"size list favorite: "+ listFavoriteArticle.size());
    }
    @Override
    public void onPageStarted(String url, Bitmap favicon) {

    }

    @Override
    public void onPageFinished(String url) {

    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }
}
