package com.example.nagat.pageclone.ultis;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.model.ModelRealm.MyMap;
import com.example.nagat.pageclone.model.ModelRealm.PassionRealm;
import com.example.nagat.pageclone.model.ModelRealm.SiteRealm;
import com.example.nagat.pageclone.model.Passion;
import com.example.nagat.pageclone.model.Site;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by nagat on 2/5/2018.
 */

public class Ultis {
    public static String getEmailUser() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
//            Log.e("Ultis.getEmailUser",FirebaseAuth.getInstance().getCurrentUser().getEmail());
            return FirebaseAuth.getInstance().getCurrentUser().getEmail();
        }
        return "";
    }

    public static RealmList<SiteRealm> convertListToRealmListSite(List<Site> list) {
        RealmList<SiteRealm> realmList = new RealmList<>();
        for (Site element : list) {
            SiteRealm realmElement = new SiteRealm();
            realmElement.setId(element.getId());
            realmElement.setAvatarSite(element.getAvatarSite());
            realmElement.setSiteName(element.getSiteName());
            realmElement.setPassionProvider(convertHashMapToRealmList(element.getPassionProvider()));
            realmList.add(realmElement);
        }
        return realmList;
    }

    public static List<Site> convertRealmListToListSite(List<SiteRealm> realmList) {
        List<Site> list = new ArrayList<>();
        for (SiteRealm siteRealm : realmList) {
            Site site = new Site();
            site.setId(siteRealm.getId());
            site.setAvatarSite(siteRealm.getAvatarSite());
            site.setSiteName(siteRealm.getSiteName());
            site.setPassionProvider(convertRealmListToHashMap(siteRealm.getPassionProvider()));
            list.add(site);
        }
        return list;
    }
    public static List<Passion> convertRealmListToListPassion(List<PassionRealm> realmList) {
        List<Passion> list = new ArrayList<>();
        for (PassionRealm siteRealm : realmList) {
            Passion passion = new Passion();
            passion.setId(siteRealm.getId());
            passion.setImage(siteRealm.getImage());
            passion.setNamePaper(siteRealm.getNamePaper());
            list.add(passion);
        }
        return list;
    }
    public static RealmList<PassionRealm> convertListToRealmListPassion(List<Passion> list) {
        RealmList<PassionRealm> realmList = new RealmList<>();
        for (Passion element : list) {
            PassionRealm passionRealm = new PassionRealm();
            passionRealm.setId(element.getId());
            passionRealm.setImage(element.getImage());
            passionRealm.setNamePaper(element.getNamePaper());
            realmList.add(passionRealm);
        }
        return realmList;
    }
    public static HashMap<String,String> convertRealmListToHashMap(RealmList<MyMap> realmList) {
        HashMap<String,String> hashMap = new HashMap<>();
        for (MyMap myMap : realmList) {
            hashMap.put(myMap.getKey(),myMap.getValue());
        }
        return hashMap;
    }
    public static RealmList<MyMap> convertHashMapToRealmList(HashMap hashMap) {
        RealmList<MyMap> realmList = new RealmList<>();
        Iterator it = hashMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            realmList.add(new MyMap((String)pair.getKey(),(String)pair.getValue()));
        }
        return realmList;
    }

    public static void tranformFragment(Context context, int containerViewId, Fragment fragment) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerViewId, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public static void tranformFragmentNotAddToStack(Context context, int containerViewId, Fragment fragment) {
        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerViewId, fragment);
        fragmentTransaction.commit();
    }
    public static void showDialog(Context context,String title,String message,DialogInterface.OnClickListener clickYes,DialogInterface.OnClickListener clickNo) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, clickYes)
                .setNegativeButton(android.R.string.no, clickNo)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    public static void showDialog(Context context,String title,String message,DialogInterface.OnClickListener clickYes) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, clickYes)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    public static void showDialog(Context context, String title, String message, DialogInterface.OnClickListener clickYes, int icon) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", clickYes)
                .setIcon(icon)
                .show();
    }

    public static String convertUrlToAPICall(String url) {
        String api = new String();
        String urlNew = url.replace("/", "%2F");
        urlNew = urlNew.replace(":", "%3A");
        api = Constant.URL_RSS2JSON + "?rss_url=" + urlNew + "&" + "api_key=" + Constant.API;
        return api;
    }

    public static String convertUrlToAPICall(String url, String orderBy, String orderDir, int count) {
        //order by theo date, title, author
        //orderdir asc or des
        //count so item tra ve
        String api = new String();
        String urlNew = url.replace("/", "%2F");
        urlNew = urlNew.replace(":", "%3A");
        api = Constant.URL_RSS2JSON + "?rss_url=" + urlNew + "&" + "api_key=" + Constant.API + "&order_by=" + orderBy + "&order_dir=" + orderDir + "&count=" + count;
        return api;
    }

    public static String howLongFrom(Date fromTime) {
        String result;
        long time = (Calendar.getInstance().getTime().getTime() - fromTime.getTime()) / 1000;
        if (time / 60 < 60) {
            result = (time / 60) + " phút trước";
        } else if (time / 3600 < 24) {
            result = (time / 3600) + " giờ trước";
        } else {
            result = (time / 86400) + " ngày trước";
        }
        return result;
    }

    public static void initDummyData(Context context) {
        BufferedReader reader = null;
        StringBuilder stringBuilder = new StringBuilder();
        String jsonStringData;
        try {
            reader = new BufferedReader(new InputStreamReader(context.getAssets().open("dummydata.json")));
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException ignored) {
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignored) {
                }
            }
        }
        jsonStringData = stringBuilder.toString();
        DummyData dummyData = new Gson().fromJson(jsonStringData, DummyData.class);
        SharedPrefs.getInstance().putArrayList(Constant.LIST_SITE, dummyData.site_list);
        SharedPrefs.getInstance().putArrayList(Constant.LIST_PASSION, dummyData.passion_list);
        SharedPrefs.getInstance().putArrayList(Constant.LIST_SITE_CHOSE, dummyData.site_list);
        SharedPrefs.getInstance().putArrayList(Constant.LIST_PASSION_CHOSE, dummyData.passion_list);

    }
}
