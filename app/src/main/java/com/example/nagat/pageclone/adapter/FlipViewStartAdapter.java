package com.example.nagat.pageclone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.model.Passion;
import com.example.nagat.pageclone.model.Site;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.DummyData;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.facebook.share.Share;

import java.util.List;

/**
 * Created by nagat on 2/5/2018.
 */

public class FlipViewStartAdapter extends BaseAdapter {
    LayoutInflater inflater;
    Context context;
    OnClickFlipView onClickFlipView;
    public void setOnClickFlipView(OnClickFlipView onClickFlipView) {
        this.onClickFlipView = onClickFlipView;
    }
    public FlipViewStartAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        this.context = context;
    }




    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    Viewholder viewholderForVisible;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Viewholder viewholder;
        int type = getItemViewType(position);
        if (convertView == null) {
            viewholder = new Viewholder();
            switch (type) {
                case 0:
                    convertView = inflater.inflate(R.layout.startup_cover1_activity, null);
                    setupView(type,convertView,viewholder);
                    break;
                case 1:
                    convertView = inflater.inflate(R.layout.startup_cover2_activity, null);
                    setupView(type,convertView,viewholder);
                    break;
                case 2:
                    convertView = inflater.inflate(R.layout.fragment_chose_site, null);
                    setupView(type,convertView,viewholder);
                    break;
                case 3:
                    convertView = inflater.inflate(R.layout.fragment_chose_passion, null);
                    setupView(type,convertView,viewholder);
                    break;
            }


            convertView.setTag(viewholder);

        } else {
            viewholder = (Viewholder) convertView.getTag();
        }
        return convertView;
    }
    private List<Passion> mListPassion;
    private List<Site> mListSite;
    private void setupView(int typeView, View view, Viewholder viewholder) {
        if (typeView == 3) {
            viewholder.gridView = view.findViewById(R.id.gridView);
            viewholder.tvDone = view.findViewById(R.id.tvDone);
//            mListPassion = DummyData.createDataPassion();
            mListPassion = SharedPrefs.getInstance().getArrayList(Constant.LIST_PASSION,Passion.class);
            GrillViewChosePassionAdapter adapter = new GrillViewChosePassionAdapter(context, mListPassion,mListPassion);
            adapter.setOnClickFlipView(new OnClickFlipView() {
                @Override
                public void onClickFlipView(int buttonClick) {

                }

                @Override
                public void onClickDone(List<Site> mListSite, List<Passion> mListPassion) {

                }

                @Override
                public void onClickGridView(int type, int position, boolean isChose,Object data) {
                    if (type == Constant.TYPE_GRIDVIEW_PASSION) {
                        if (isChose) {
                            mListPassion.add(position, (Passion) data);
                        } else {
                            mListPassion.remove(position);
                        }
                    }
                    if (type == Constant.TYPE_GRIDVIEW_SITE) {
                        if (isChose) {
                            mListSite.add(position, (Site) data);
                        } else {
                            mListSite.remove(position);
                        }
                    }
                }
            });
            viewholder.gridView.setAdapter(adapter);
            viewholder.gridView.setOnTouchListener(new View.OnTouchListener(){

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return event.getAction() == MotionEvent.ACTION_MOVE;
                }

            });
            viewholder.tvDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickFlipView.onClickDone(mListSite,mListPassion);
                }
            });
        }
        if (typeView == 2) {
            viewholder.gridView = view.findViewById(R.id.gridView);
            mListSite = SharedPrefs.getInstance().getArrayList(Constant.LIST_SITE,Site.class);
            GrillViewChoseSiteAdapter adapter = new GrillViewChoseSiteAdapter(context, mListSite,mListSite);
            adapter.setOnClickFlipView(new OnClickFlipView() {
                @Override
                public void onClickFlipView(int buttonClick) {

                }

                @Override
                public void onClickDone(List<Site> mListSite, List<Passion> mListPassion) {

                }

                @Override
                public void onClickGridView(int type, int position, boolean isChose,Object data) {
                    if (type == Constant.TYPE_GRIDVIEW_PASSION) {
                        if (isChose) {
                            mListPassion.add(position, (Passion) data);
                        } else {
                            mListPassion.remove(position);
                        }
                    }
                    if (type == Constant.TYPE_GRIDVIEW_SITE) {
                        if (isChose) {
                            mListSite.add(position, (Site) data);
                        } else {
                            mListSite.remove(position);
                        }
                    }
                }
            });
            viewholder.gridView.setAdapter(adapter);
            viewholder.gridView.setOnTouchListener(new View.OnTouchListener(){

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return event.getAction() == MotionEvent.ACTION_MOVE;
                }

            });
        }
        if (typeView == 1) {
            viewholder.viewBatDauCover1 = view.findViewById(R.id.btBatDau);
            viewholder.viewDangNhapCover1 = view.findViewById(R.id.btDangNhap);
            if (SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false)) {
                viewholder.viewDangNhapCover1.setVisibility(View.GONE);
            }
            viewholderForVisible = viewholder;
            viewholder.viewDangNhapCover1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickFlipView.onClickFlipView( Constant.BUTTON_DANGNHAP);
                }
            });
            viewholder.viewBatDauCover1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickFlipView.onClickFlipView(Constant.BUTTON_BATDAU);
                }
            });
        }
    }
    public void setVisibleDangNhap() {
        viewholderForVisible.viewDangNhapCover1.setVisibility(View.GONE);
    }
    class Viewholder {
        GridView gridView;
        View viewBatDauCover1;
        View viewDangNhapCover1;
        TextView tvDone;
    }
}
