package com.example.nagat.pageclone.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.nagat.pageclone.BaseActivity;
import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.adapter.FlipViewStartAdapter;
import com.example.nagat.pageclone.adapter.OnClickFlipView;
import com.example.nagat.pageclone.model.ModelRealm.PassionRealm;
import com.example.nagat.pageclone.model.ModelRealm.SiteRealm;
import com.example.nagat.pageclone.model.Passion;
import com.example.nagat.pageclone.model.Site;
import com.example.nagat.pageclone.model.User;
import com.example.nagat.pageclone.realm.RealmController;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.DummyData;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.example.nagat.pageclone.ultis.Ultis;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import se.emilsjolander.flipview.FlipView;

/**
 * Created by nagat on 2/5/2018.
 */

public class StartActivity extends BaseActivity {
    FlipView flipView;
    Realm realm;
    FlipViewStartAdapter adapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        realm = RealmController.with(this).getRealm();
        flipView = (FlipView) findViewById(R.id.flip_view);
        adapter = new FlipViewStartAdapter(this);
        adapter.setOnClickFlipView(new OnClickFlipView() {
            @Override
            public void onClickFlipView(int buttonClick) {
                if (buttonClick == Constant.BUTTON_DANGNHAP) {
                    Intent intent = new Intent(StartActivity.this, LoginActivity.class);
                    startActivityForResult(intent,1);

                }
                if (buttonClick == Constant.BUTTON_BATDAU) {
                    flipView.smoothFlipTo(2);
                }
            }

            @Override
            public void onClickDone(List<Site> mListSiteChose, List<Passion> mListPassionChose) {
                SharedPrefs.getInstance().putArrayList(Constant.LIST_PASSION_CHOSE,mListPassionChose);
                SharedPrefs.getInstance().putArrayList(Constant.LIST_SITE_CHOSE,mListSiteChose);
                if (SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false)) {
                    User user = RealmController.getInstance().getUser(Ultis.getEmailUser());
                    User user1 = new User();
                    user1.setEmail(user.getEmail());
                    user1.setDisplayName(user.getDisplayName());
                    user1.setFavoriteSite(Ultis.convertListToRealmListSite(mListSiteChose));
                    user1.setFavoritePassion(Ultis.convertListToRealmListPassion(mListPassionChose));
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(user1);
                    realm.commitTransaction();

                }
                Intent intent = new Intent(StartActivity.this, MainActivity.class);
                SharedPrefs.getInstance().put(Constant.FIRST_TIME_RUN,false);
                startActivity(intent);
                finish();

            }

            @Override
            public void onClickGridView(int type, int position, boolean isChose, Object data) {

            }


        });
        flipView.setAdapter(adapter);
        dummydataForTest();
    }
    private void dummydataForTest() {
        User user = new User();
        user.setDisplayName("test");
        user.setEmail("test@gmail.com");
        user.setFavoriteSite(Ultis.convertListToRealmListSite(SharedPrefs.getInstance().getArrayList(Constant.LIST_SITE,Site.class)));
        user.setFavoritePassion(Ultis.convertListToRealmListPassion(SharedPrefs.getInstance().getArrayList(Constant.LIST_PASSION,Passion.class)));
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();
    }
    @Override
    protected void onStart() {
        super.onStart();
        if (SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false)) {
            if (RealmController.with(this).getUser(Ultis.getEmailUser()).getFavoriteSite().size() >0) {
                Intent intent = new Intent(StartActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                flipView.smoothFlipTo(2);
                adapter.setVisibleDangNhap();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}
