package com.example.nagat.pageclone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nagat.pageclone.model.Passion;

import java.util.ArrayList;
import java.util.List;

import com.example.nagat.pageclone.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by DUYPC on 4/24/2017.
 */

public class AdapterGridViewMain extends BaseAdapter {

    private Context mContext;
    private List<Passion> gridviewInfos;
    LayoutInflater inflater;
    OnClick onClick;
    public void setOnClick(OnClick onClick) {
        this.onClick = onClick;
    }
    public AdapterGridViewMain(Context context, List<Passion> list){
        mContext = context;
        gridviewInfos = list;
        inflater = (LayoutInflater)context.getApplicationContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return gridviewInfos.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Passion gridviewInfo = gridviewInfos.get(position);
        ViewHolder viewHolder;
        if (convertView == null){
            convertView =  inflater.inflate(R.layout.row_girdview,null);
            viewHolder = new ViewHolder();
            viewHolder.imageView =  convertView.findViewById(R.id.avatarNewspaper);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.nameNewspaper);
//            convertView.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 450));
            convertView.setTag(viewHolder);
        }else {
            //Log.e("khanhduy.le","tai su dung");
            viewHolder = (ViewHolder) convertView.getTag();
        }

//        viewHolder.imageView.setImageResource(gridviewInfo.getImage());
        viewHolder.textView.setText(gridviewInfo.getNamePaper());
        Picasso.get()
                .load(gridviewInfo.getImage())
                .placeholder(R.drawable.ic_white_2x)
                .error(R.drawable.ic_white_2x)
                .resize(65,65)
                .centerCrop()
                .into(viewHolder.imageView);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onClickView(gridviewInfo);
            }
        });
        return convertView;
    }


    private class ViewHolder{
        public TextView textView;
        public CircleImageView imageView;
    }
}
