package com.example.nagat.pageclone.adapter;

import android.content.Context;
import android.media.Image;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.model.Article;
import com.example.nagat.pageclone.model.ModelRealm.ArticleRealm;
import com.example.nagat.pageclone.model.model_rss.Item;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.example.nagat.pageclone.ultis.Ultis;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by nagat on 8/5/2018.
 */

public class FlipViewReviewNewsAdapter extends BaseAdapter {
    private static final String TAG = "FlipViewReviewNewsAdapter";
    LayoutInflater inflater;
    Context context;
    List<Article> items;
    List<ArticleRealm> listFavorite;
    OnClick onClick;
    public void setOnClick(OnClick onClick) {
        this.onClick = onClick;
    }
    public FlipViewReviewNewsAdapter(Context context, List<Article> items, List<ArticleRealm> listFavorite) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.listFavorite = listFavorite;
    }
    public void setListFavorite(List<ArticleRealm> listFavorite) {
        this.listFavorite = listFavorite;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Article item = items.get(position);
        final ViewHolder viewHolder;
        if (convertView == null){
            convertView =  inflater.inflate(R.layout.item_review_news,null);
            viewHolder = new ViewHolder();
            viewHolder.thumbnail =  convertView.findViewById(R.id.thumbnail);
            viewHolder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            viewHolder.tv_review =  convertView.findViewById(R.id.review);
            viewHolder.tv_author = convertView.findViewById(R.id.tv_author);
            viewHolder.tv_time = convertView.findViewById(R.id.tv_time);
            viewHolder.iv_favorite = convertView.findViewById(R.id.iv_favorite);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (item.getImage().trim().isEmpty()){
            viewHolder.thumbnail.setImageResource(R.drawable.ic_white_2x);
        } else {
            Picasso.get()
                    .load(item.getImage())
                    .placeholder(R.drawable.ic_white_2x)
                    .error(R.drawable.ic_white_2x)
                    .into(viewHolder.thumbnail);
        }
        Log.e("TAG","onGetView" +listFavorite.size());
        viewHolder.tv_title.setText(item.getTitle());
        viewHolder.tv_time.setText(Ultis.howLongFrom(new Date(item.getPubDate())));
        viewHolder.tv_author.setText(item.getAuthor());
        viewHolder.tv_review.setText(item.getDescription());
        viewHolder.iv_favorite.setImageResource(R.drawable.favorite);
        viewHolder.isFavorite = false;
        viewHolder.articleRealm = new ArticleRealm();
        viewHolder.articleRealm.setTitle(item.getTitle());
        viewHolder.articleRealm.setAuthor(item.getAuthor());
        viewHolder.articleRealm.setContent(item.getContent());
        viewHolder.articleRealm.setDescription(item.getDescription());
        viewHolder.articleRealm.setLink(item.getLink());
        viewHolder.articleRealm.setImage(item.getImage());
        viewHolder.articleRealm.setPubDate(item.getPubDate());
//        Log.e(TAG,"size list favorite: "+listFavorite.size());
        for (ArticleRealm articleRealm : listFavorite) {
            if (item.getLink().equals(articleRealm.getLink())) {
                viewHolder.iv_favorite.setImageResource(R.drawable.favorite_clicked);
                viewHolder.isFavorite = true;
            }
        }
        viewHolder.iv_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.isFavorite) {
                    viewHolder.isFavorite = false;
                    viewHolder.iv_favorite.setImageResource(R.drawable.favorite);
                    onClick.onClickFavorite(viewHolder.articleRealm ,false);
                } else {
                    viewHolder.isFavorite = true;
                    viewHolder.iv_favorite.setImageResource(R.drawable.favorite_clicked);
                    onClick.onClickFavorite(viewHolder.articleRealm ,true);
                }
            }
        });
        if (SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false)) {
            viewHolder.iv_favorite.setVisibility(View.VISIBLE);
        } else {
            viewHolder.iv_favorite.setVisibility(View.GONE);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onClickReviewNews(item);
            }
        });
        return convertView;
    }


    class ViewHolder  {
        ImageView thumbnail;
        TextView tv_title;
        TextView tv_review;
        TextView tv_author;
        TextView tv_time;
        ImageView iv_favorite;
        boolean isFavorite;
        ArticleRealm articleRealm;
    }
}
