package com.example.nagat.pageclone.model;

import java.io.Serializable;

/**
 * Created by nagat on 18/4/2018.
 */

public class Passion implements Serializable{
    private String namePaper;
    private String image;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Passion() {
    }

    public Passion(String namePaper, String image, String id) {
        this.namePaper = namePaper;
        this.image = image;
        this.id = id;

    }

    public String getNamePaper() {
        return namePaper;
    }

    public void setNamePaper(String namePaper) {
        this.namePaper = namePaper;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Passion passion = (Passion) o;

        return namePaper.equals(passion.namePaper);
    }

    @Override
    public int hashCode() {
        return namePaper.hashCode();
    }
}
