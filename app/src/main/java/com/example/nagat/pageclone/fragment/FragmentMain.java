package com.example.nagat.pageclone.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.nagat.pageclone.BaseFragment;
import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.adapter.AdapterGridViewMain;
import com.example.nagat.pageclone.adapter.OnClick;
import com.example.nagat.pageclone.model.Article;
import com.example.nagat.pageclone.model.ModelRealm.ArticleRealm;
import com.example.nagat.pageclone.model.ModelRealm.PassionRealm;
import com.example.nagat.pageclone.model.Passion;
import com.example.nagat.pageclone.model.model_rss.Item;
import com.example.nagat.pageclone.realm.RealmController;
import com.example.nagat.pageclone.ui.MainActivity;
import com.example.nagat.pageclone.ui.ReviewNewsActivity;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.example.nagat.pageclone.ultis.Ultis;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by nagat on 3/5/2018.
 */

public class FragmentMain extends BaseFragment {
    private static final String TAG = "FragmentMain";
    private List<Passion> genresList;
    GridView girdView;
    Realm realm;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_main,container,false);
        ((MainActivity) getActivity()).choseItem(R.id.nav_home);
        realm = RealmController.with(this).getRealm();
        genresList = new ArrayList<>();
        if (SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false)) {
            List<PassionRealm> realmRealmList;
            realm.beginTransaction();
            realmRealmList = realm.copyFromRealm(RealmController.with(this).getUser(Ultis.getEmailUser()).getFavoritePassion());
            realm.commitTransaction();
            genresList = Ultis.convertRealmListToListPassion(realmRealmList);
        } else {
            if (SharedPrefs.getInstance().getArrayList(Constant.LIST_PASSION_CHOSE,Passion.class)!=null) {
                genresList = SharedPrefs.getInstance().getArrayList(Constant.LIST_PASSION_CHOSE,Passion.class);
            }
        }

        girdView = (GridView) view.findViewById(R.id.gridview);
        AdapterGridViewMain adapterGridViewMain = new AdapterGridViewMain(getContext(), genresList);
        adapterGridViewMain.setOnClick(new OnClick() {


            @Override
            public void onClickView(Passion passion) {
                Intent intent = new Intent(getContext(), ReviewNewsActivity.class);
                intent.putExtra(Constant.PASSION,passion);
                startActivity(intent);
            }

            @Override
            public void onClickReviewNews(Article item) {

            }

            @Override
            public void onClickFavorite(ArticleRealm item, Boolean isFavorite) {

            }


        });
        girdView.setAdapter(adapterGridViewMain);
        return view;
    }
}
