package com.example.nagat.pageclone.network;

import com.example.nagat.pageclone.model.model_rss.Item;
import com.example.nagat.pageclone.model.model_rss.ModelRss;

import java.util.List;

/**
 * Created by nagat on 8/5/2018.
 */

public interface OnLoadRss {
    void onSuccessLoadRss(ModelRss modelRss);
    void onPreExecute();
}
