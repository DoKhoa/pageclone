package com.example.nagat.pageclone.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.example.nagat.pageclone.BaseFragment;
import com.example.nagat.pageclone.R;
import com.example.nagat.pageclone.adapter.GrillViewChosePassionAdapter;
import com.example.nagat.pageclone.adapter.OnClickFlipView;
import com.example.nagat.pageclone.model.ModelRealm.PassionRealm;
import com.example.nagat.pageclone.model.Passion;
import com.example.nagat.pageclone.model.Site;
import com.example.nagat.pageclone.model.User;
import com.example.nagat.pageclone.realm.RealmController;
import com.example.nagat.pageclone.ultis.Constant;
import com.example.nagat.pageclone.ultis.DummyData;
import com.example.nagat.pageclone.ultis.SharedPrefs;
import com.example.nagat.pageclone.ultis.Ultis;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by nagat on 2/5/2018.
 */

public class FragmentChosePassion extends BaseFragment{
    private GridView gridView;
    private List<Passion> mListPassion;
    private List<Passion> mListPassionChose;
    private TextView btDone;
    private Realm realm;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chose_passion, container, false);
        gridView = view.findViewById(R.id.gridView);
        btDone  = view.findViewById(R.id.tvDone);
        btDone.setVisibility(View.VISIBLE);
        realm = RealmController.with(this).getRealm();
        btDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false)) {
                    User user = RealmController.with(FragmentChosePassion.this).getUser(Ultis.getEmailUser());
                    User user1 = new User();
                    user1.setEmail(user.getEmail());
                    user1.setDisplayName(user.getDisplayName());
                    user1.setFavoriteSite(user.getFavoriteSite());
                    user1.setFavoritePassion(Ultis.convertListToRealmListPassion(mListPassionChose));
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(user1);
                    realm.commitTransaction();

                } else {
                    SharedPrefs.getInstance().putArrayList(Constant.LIST_PASSION_CHOSE,mListPassionChose);
                }
                final FragmentMain fragmentMain = new FragmentMain();
                Ultis.showDialog(getContext(), "Chỉnh sửa chủ đề", "Hoàn tất chỉnh sửa", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Ultis.tranformFragmentNotAddToStack(getContext(),R.id.full,fragmentMain);
                    }
                },R.drawable.icons8_ok);
            }
        });
        if (!SharedPrefs.getInstance().get(Constant.LOGIN,Boolean.class,false)) {
            if (SharedPrefs.getInstance().getArrayList(Constant.LIST_PASSION_CHOSE,Passion.class)!=null) {
                mListPassionChose = SharedPrefs.getInstance().getArrayList(Constant.LIST_PASSION_CHOSE,Passion.class);
            }
        } else {
            List<PassionRealm> realmRealmList;
            realm.beginTransaction();
            realmRealmList = realm.copyFromRealm(RealmController.with(this).getUser(Ultis.getEmailUser()).getFavoritePassion());
            realm.commitTransaction();
            mListPassionChose = Ultis.convertRealmListToListPassion(realmRealmList);
        }

        mListPassion = SharedPrefs.getInstance().getArrayList(Constant.LIST_PASSION,Passion.class);
        GrillViewChosePassionAdapter adapter = new GrillViewChosePassionAdapter(getActivity(),mListPassion,mListPassionChose);
        adapter.setOnClickFlipView(new OnClickFlipView() {
            @Override
            public void onClickFlipView(int buttonClick) {

            }

            @Override
            public void onClickDone(List<Site> mListSite, List<Passion> mListPassion) {

            }

            @Override
            public void onClickGridView(int type, int position, boolean isChose, Object data) {
                if (type == Constant.TYPE_GRIDVIEW_PASSION) {
                    if (isChose) {
                        mListPassionChose.add((Passion) data);
                    } else {
                        mListPassionChose.remove((Passion) data);
                    }
                }

            }
        });
        gridView.setAdapter(adapter);
//        this.setupUI(getActivity().getWindow().getDecorView().findViewById(android.R.id.content));
        return view;

    }


}
