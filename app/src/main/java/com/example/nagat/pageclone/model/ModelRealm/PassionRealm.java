package com.example.nagat.pageclone.model.ModelRealm;

import com.example.nagat.pageclone.model.Passion;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nagat on 11/5/2018.
 */

public class PassionRealm extends RealmObject {
    private String namePaper;
    private String image;
    @PrimaryKey
    private String id;

    public PassionRealm() {
    }

    public String getNamePaper() {
        return namePaper;
    }

    public void setNamePaper(String namePaper) {
        this.namePaper = namePaper;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PassionRealm passion = (PassionRealm) o;

        return namePaper.equals(passion.namePaper);
    }

    @Override
    public int hashCode() {
        return namePaper.hashCode();
    }
}
